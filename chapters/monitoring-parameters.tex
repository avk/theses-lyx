\chapter{Description Parameters}
\label{chapter:description-parameters}

The main motivation behind this thesis is to learn more about the scheduler and the relation between the valid/invalid task configurations and the tasks. We want to generate a pareto-optimal valid initial mapping along with possible valid and invalid mappings. These valid and invalid mappings along with the task monitoring data collected during the process will be used to generate knowledge. This knowledge can then be used in the online phase to dynamically reconfigure the system based on the requirements of the system and its running environment.

One of the biggest question that should be raised is, "What are the several parameters that a task has to provide to the scheduler?". These parameters, on the one hand, help the scheduler to make better decisions, but on the other hand, they are very important to find the relations and patterns in our knowledge for dynamic mappings.  Another question that should be raised is, "What are the parameters the hardware should provide to the scheduler?". The benefits of this are similar to those of the task parameters. In this thesis, a list of tasks with the parameters specified is termed as \textbf{Task Description}. Thus, the parameters are also called as Task Description Parameters on some occasions.

This chapter discusses the findings of our research on these parameters. It is divided into two parts; the first part discusses the parameters that a task can provide to the scheduler. The second part discusses the parameters that the hardware (or the \gls{ecu}) can provide.

It is important to describe the nature of the parameters before we discuss the parameters. In most software applications, the requirements of the system can be distinguished into functional and non-functional requirements. On one hand, functional requirements deal with everything that is necessary for the system to function correctly on an algorithmic level. The only dependency for a system to be functionally correct is in the input data and the mathematics behind the calculation involved. On the other hand, non-functional requirements deal with everything that doesn't directly affect the output of the data. The parameters that we have researched are mostly the non-functional requirements for the task. Similarly, for the \gls{ecu}, the parameters are mostly the capabilities that describe the current state of the system and the resources that it could provide. These capabilities are in most cases analogous to the non-functional requirements of the tasks.

\section{Task Description Parameters}
\label{sec:taskParameters}
There are number of parameters that a task should provide. Some of them are related to the hardware while some are related to the relation of the task with other tasks. We have categorized these parameters based on their relation to the system and the significance of the parameter.  The parameters and their basic descriptions are mentioned below:

\subsection{Scheduling Data}
  \begin{description}
    \item{\textbf{Arrival Time}} \textendash{} Arrival time is the point of time when the task is activated in the system.
    \item{\textbf{\gls{wcet}}} \textendash{} \gls{wcet} is the time that a task takes to execute in the worse case scenario.
    \item{\textbf{Amount of Threads}} \textendash{} The number of threads that a task creates or requires.
    \item{\textbf{Capabilities}} \textendash{} Capabilities is sometimes related to the Fiasco.OC/L4Re system. They define the type of access a task needs from a service or the operating system or a kernel object, for example: a task needs network access.
    \item{\textbf{Deadline}} \textendash{} The deadline is the time frame within which the task has to complete execution. If the task is not completed within the deadline period then the execution of the task or the output that is provided by the task is not useful to the system anymore.
  \end{description}

\subsection{Hardware Resources}
  \begin{description}
\item{\textbf{Processing Load}} \textendash{} Processing load is the number of CPU cycles that is required by the task for successful execution.
\item{\textbf{Memory Requirements}} \textendash{} Memory requirements describes the demand of dynamic memory (RAM) and storage memory (ROM) required during the execution of the task.
\item{\textbf{Communication}} \textendash{} Communication denotes if the task requires communication or not.
\item{\textbf{Lowest Required Power}} \textendash{} This is the lowest power state in which the task would be active.
\item{\textbf{Required Processor Architecture}} \textendash{} This describes the processor architectures that the task would successfully run on. Or on the contrary it would describe the required modules from a processor that would be essential for successful execution.\ For example: availability of a digital signal processor (DSP).
  \end{description}

\subsection{Dependencies}
  \begin{description}
\item{\textbf{Dependency}} \textendash{} Dependency describes if the task is dependent on other tasks or not, and if so, it notifies how it is dependent on the other tasks.
\item{\textbf{Co-Location Restriction}} \textendash{} Co-Location restriction denotes if a task has to be located on the same \gls{ecu} as another task for successful execution.
  \end{description}

\subsection{Miscellaneous}
  \begin{description}
    \item{\textbf{\gls{asil}}} \textendash{} \gls{asil} is a risk classification standard defined by the Functional Safety for Road Vehicles standard. \gls{sil} is assigned to system tasks and eventually to architecture components of the system\cite{papadopoulos2010automatic}.
    \item{\textbf{Sensor Access}} \textendash{} Sensor Access lists the sensors that the task requires access to.
    \item{\textbf{Actuator Access}} \textendash{} Actuator Access lists the actuators that the task requires access to.
  \end{description}

\section{\gls{ecu} Parameters}
Just like the task providing its requirement parameters to the scheduler, there are number of parameters that it learns from the \gls{ecu} as well. These parameters have been categorized based on the nature of the parameters. The parameters and their basic descriptions are mentioned below:


\subsection{Hardware Resources}
  \begin{description}
    \item{\textbf{Processing Power}} \textendash{} Each \gls{ecu} has some processing capacity, which is one of the most important non-functional requirements of a task. This generally denotes the amount of processing power that is available at a particular point of time rather than the total processing power of the \gls{ecu}.
    \item{\textbf{Memory}} \textendash{} Similar to the processing power, memory is also one of the most important non-functional requirements of a task. This also describes the memory that is available at a time. It can also be divided into available dynamic memory (i.e. RAM) and available permanent memory (i.e. ROM).
    \item{\textbf{Power State}} \textendash{} In most cases real time and embedded systems are deployed in a environment with lot of power constraints (i.e.\ they have to cope with limited power supply). Because of this, there are number of optimizations that a system employs to save power because of which an \gls{ecu} can be in a different power state depending on the state of the system and the tasks deployed to it.
    \item{\textbf{Availability Time}} \textendash{} There are number of tasks assigned to an \gls{ecu} at any point of time. The availability time is the earliest time an \gls{ecu} can complete the execution of all the tasks that have been previously assigned to it.
    \item{\textbf{Architecture}} \textendash{} As we discussed that tasks can have a dependency on the type of the processor architecture it runs on, it is one of the parameters that the \gls{ecu} provides to the scheduler.
  \end{description}

\subsection{Performance}
  \begin{description}
    \item{\textbf{Network Delay} \textendash{}} Network Delay describes the reliability of the network the \gls{ecu} is connected to.
    \item{\textbf{Failure Rate} \textendash{}} Failure rate describes the reliability of the \gls{ecu}.
  \end{description}

\subsection{Miscellaneous}
  \begin{description}
    \item{\textbf{\gls{asil}}} \textendash{} Similar to the \gls{asil} of tasks, there is an \gls{asil} of each processing core as well. It describes the safety and integrity level of the processing core based on which several tasks can or cannot be assigned to it.
    \item{\textbf{Sensor Access}} \textendash{} Sensor Access lists the sensors that the \gls{ecu} has access to.
    \item{\textbf{Actuator Access}} \textendash{} Actuator Access lists the actuators that the \gls{ecu} has access to.
  \end{description}
