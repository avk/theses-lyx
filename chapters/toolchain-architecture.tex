\chapter{Toolchain Design And Architecture}
\label{chapter:toolchain_design_and_architecture}

This chapter introduces the high level architecture of the toolchain. We follow a bottom-up approach and start with the functional and non-functional considerations of the toolchain. This first part of the chapter discusses the several alternatives we researched for the implementation of the toolchain. Then, we discuss the division of the toolchain into sub-components. This division of components is based on the functionality and the location where the component would be placed. Finally, we describe the significance of each of the components and the functions that they play as a part of the toolchain.

\section{Requirements And Alternatives}

The main requirement of the toolchain is to generate data about possible deployments and monitoring data associated from them. The deployments would be mostly random, but have some basic constraints, for example: ASIL/Safety Level, i.e.\ Tasks of different level of safety cannot be on the same ECU.\ Since, the requirements were not very constrained, there were number of possible options to achieve them; some of the possible options are discussed below:

\begin{figure}
  \includegraphics[width=1\textwidth]{figures/iterative-model}
  \caption{Plan for building the system iteratively}
\label{fig:iterativeModelBuilding}
\end{figure}

  \begin{description}
    \item{\textbf{Simulation / Design Space Exploration tools}} \textendash{} Design space exploration is the activity of exploring design alternatives prior to implementation\cite{kang2011approach}. Since, this is exactly the goal of the system i.e.\ to explore different possible deployment options before any implementation, we evaluate number of possible tools and frameworks that are already available; Tools that we can use to design our system and generate the data. Among the number of tools we found, Autofocus3 was one of the tools that fit our requirements the most, so we did a deeper analysis of the tool and analyzed if it fit to our requirements.

      Autofocus3 is a design space exploration tool to develop embedded systems using models from the requirements to the hardware architecture, passing by the design of the logical architecture, the deployment and the scheduling~\cite{af3web}. The tool has number of features like modeling and simulation which is quite good because we can then model the system in the tool and then simulate different scheduling algorithms to generate the data. There are number of limitations that the tool has in the context of our requirement specification and context. These limitation are mentioned below:

      \begin{itemize}
        \item Modeling of the \gls{ecu} or the hardware is a complex task and is not well supported by the system.
        \item Lack of real monitoring data, as most of the parameters we would receive would be simulated and calculated mathematically. We want a more empirical approach that is close to the final deployment as knowledge would make the most sense in that case.
        \item Lack of real-time behavior because of simulation.
      \end{itemize}

  \item{\textbf{Emulation}} \textendash{} Since the issue with simulation is that the monitoring data would not be generated, introducing an emulated system running a microkernel based operating system best solves it. This approach introduces the complexity of using a separate tool to generate and send the tasks to the system and similarly, another tool that could log all the monitored data back from the system. This approach gives us the advantage that we can test the toolchain in an environment that can be quite close to the final deployment without having extra hardware involved.

  \item{\textbf{Hardware Interloop}} \textendash{} This is the best-case scenario where the system would be running in a hardware and be communicating with the tool by receiving task descriptions and returning monitoring data. This approach would guarantee the best-monitored data from the kernel. While this approach has it benefits, the biggest issue of this approach is that it adds extra complexity of the hardware and the system not having the proper drivers to communicating with the system.
  \end{description}

  It can be seen from the options above that the Design Space Exploration approach doesn't fit our requirement spec and the hardware interloop approach requires more time and effort, which is outside the scope of the thesis. As represented visually in figure~\ref{fig:iterativeModelBuilding}, we have an iterative approach to building the system. We start with emulating a single instance of the microkernel and build the toolchain so that it works with it. This step would ensure that the toolchain is complete and a functioning monitoring loop is in place. The toolchain will then be extended to work with a network of emulated microkernel instances. The final step would be to replace the emulated network of microkernel instances with the instances running in dedicated hardware connected physically in the network. This approach clearly has the following benefits:

  \begin{itemize}
    \item There are no hardware dependencies when architecting and prototyping the system in the beginning, and
    \item It gives us enough flexibility to extend it once a basic working system is available.
  \end{itemize}

    In this way, the system is designed so that there are minimum dependencies in the beginning and iteratively the system can be extended to be close to the visionary scenario the system wants to be in.

\section{Toolchain Architecture}
\label{toolchain-architecture}
On a high level, the toolchain is divided into two distinct components, the PC component and the microkernel component. The PC component is responsible for generating the tasks and communicating with the microkernel component. Since, it is run on the PC, thus it is called as the PC component. The microkernel component on the other hand consists of all the sub-components that run in the microkernel. It is responsible for managing the tasks in the microkernel and managing the monitoring communication with the PC component. The different sub-components of the toolchain and their communication is visually represented in figure~\ref{fig:high-level-architecture}.

% XXX: include database in this diagram.
\begin{figure}
\includegraphics[width=1\textwidth]{figures/high-level-architecture}
\caption{High Level Architecture of the System}
\label{fig:high-level-architecture}
\end{figure}

\subsection{PC Component}
\label{sec:pc-component}
The PC component has the following specific tasks:
\begin{description}
  \item{\textbf{Generation of task descriptions}} \textendash{} The generator generates a set of task description, each of which consists of a task with the parameters described in Chapter~\ref{chapter:description-parameters}.
  \item{\textbf{Generation of task binaries}} \textendash{} Based on the tasks enlisted in the task description, executable binaries of the tasks have to be generated. These tasks will then be sent to the microkernel component to run.
  \item{\textbf{Communicate with the microkernel component}} \textendash{} The PC component would send the task descriptions and the task binaries to the microkernel component. Once the microkernel starts executing the tasks it replies with the monitoring data in a timely basis.
  \item{\textbf{Save monitoring data}} \textendash{} Finally, the PC component should save the monitoring data in a structured format so that some data mining and pattern matching algorithms can be applied on it moving forward. It is used to generate the knowledge as mentioned in Section~\ref{sec:motivation}.
\end{description}

It is a design decision to generate/compile the task binaries in the PC component. The approach would enable the system to work with any microkernel instance, as it would not have to be bundled with the binaries before initialization. Moreover, this design decision gives us the flexibility to change the type of tasks and the load generated by tasks as per the requirement of the system.

Since the PC component has number of distinct tasks, it is divided into the following sub-components.
\begin{itemize}
  \item Task Generation component
  \item Networking component
\end{itemize}

\subsubsection{Task Generation Component}
The task generation component is responsible for generating task descriptions. The input to this component is the number of tasks to be generated and the output is a set of tasks with random task descriptions. The task descriptions have parameters as discussed in {{monitoring parameters section}}.

\subsubsection{Networking Component}
The networking component is responsible for communicating with the microkernel component. The communication between the networking component and the microkernel component can be divided into three specific categories.
  \begin{description}
\item{\textbf{Task Descriptions}} \textendash{} The networking subcomponent is responsible to initiate the communication with the microkernel component and send the task description that has been generated by the task generation component.
\item{\textbf{Task Binaries}} \textendash{} Since, the binaries are generated in the PC component, the component sends binaries to the microkernel component. As a part of this task it is also important to communicate the name of the binary to make them addressable and initiate the execution separately.
\item{\textbf{Monitoring}} \textendash{} Apart from sending the task descriptions and task binaries, the component is also responsible for receiving the monitoring data from the microkernel component. The monitoring data is then stored for processing in a later stage.
  \end{description}

\subsection{Microkernel Component}
\label{sec:microkernel-component}
The microkernel component has the following tasks:
\begin{itemize}
  \item Accept incoming task descriptions,
  \item Parse and interpret task descriptions,
  \item Activate/Start the tasks, and
  \item Collect monitoring data and send it to the PC component.
\end{itemize}

Similar to the PC component, the microkernel component can also be divided into following two distinct sub-components.
\begin{description}
\item{\textbf{Networking Component}} \textendash{} Named appropriately, the networking component is responsible for everything related to communicating with the PC component. Thus, it takes the task descriptions and task binaries, and sends back monitoring data. Apart from communication, it is also responsible to activate the tasks in the microkernel system and access the monitoring data from the monitoring component. Since it is also responsive to activate the tasks in the microkernel system, it is also responsible to manage the binaries received over the network and communicates with the rest of the system whenever required.
\item{\textbf{Monitoring Component}} \textendash{} The monitoring component on the other hand is solely responsible to access the microkernel internal objects and return monitoring data whenever requested.
\end{description}
