\chapter{Prototypical Implementation Of Toolchain}
\label{chapter:prototypical_implementation_of_toolchain}

This chapter gives a detailed account of the implementation of the toolchain. It discusses implementation details, code examples and design decisions made during implementation process. The structure of this chapter follows the architecture as presented in Chapter~\ref{chapter:toolchain_design_and_architecture}. The first section discusses the implementation of the several sub-components in the PC component. Then the different sub-components involved in the microkernel component are discussed. The following section focuses on the network architecture used for the prototyping process. Finally, since the toolchain has many sub-components, the last section of this chapter discusses the build environment used to setup and run the toolchain.

\section{PC Component}
The PC component is responsible for generation of task descriptions, compiling the task binaries and communicating with the microkernel component. The task generation component is based on a third-party framework called \emph{tms-sim}. The \emph{tms-sim} tool has been extended based on our requirements of task and its parameters. The communication with the microkernel is handled by a network tool. The network tool is built using the node.js runtime. The network tool is also dependent on various third-party packages of \emph{node.js}; these packages differ in their significance as some take care of the low-level network communication while some packages are there just to give the tool a better architecture in terms of code. The compiling of task binaries is mostly done using the toolchain that has been provided by the microkernel and it is facilitated by a build script, which is discussed in Section~\ref{sec:buildTooling}.

  \subsection{Task Generation Component: tms-sim}
  \label{sec:tmssim}
  On a very high level, \emph{tms-sim} is a C++ framework for the simulation and evaluation of real-time scheduling algorithms \cite{tmssimweb}. The core of the framework provides base classes that are used to represent tasks. Several types of task models like periodic tasks and sporadic tasks have been implemented in the framework. During the work of the thesis, the Task class is extended to implement a \textbf{PeriodicLoadTask} class that would be representing different parameters of a task as discussed in Section~\ref{sec:taskParameters}. It can be seen in figure~\ref{fig:tmsSimClassDiagram} how the different classes are related and how the \textbf{PeriodicLoadTask} class fits in the class hierarchy.

  It can also be seen in figure~\ref{fig:tmsSimClassDiagram} that the \textbf{Task} class implements the \textbf{WriteableToXML} interface that enables the task classes to be exported into \gls{xml} files.\ The \textbf{WriteableToXML} class defines several methods which are used to serialize any object type to \gls{xml}.

  Similar to the task models, the \emph{tms-sim} framework is also bundled with a \textbf{MKGenerator} class. This class is used to randomly generate real-time task sets \cite{klugetms}. During the generation of the task-sets, each task is assigned a randomly selected period, the criteria for the period being that it falls under the provided range of minimum and maximum period.

  Thus, by extending \emph{tms-sim} and adding a new task model to it, we have a task generator that generates random tasks with provided task descriptions. The best part is that moving forward we could also use the simulation and evaluation features of tms-sim to simulate the random task sets we generate and evaluate them based on how they perform in the microkernel instances.

  \begin{figure}
  \includegraphics[width=1\textwidth]{figures/tms-sim-class-diagram}
  \caption{Class hierarchy of task implementations}
  \label{fig:tmsSimClassDiagram}
  \end{figure}

  \subsection{Network Component}
  The network component is responsible for initiating and managing communication with the microkernel component. It is built using the \emph{node.js} runtime environment. Node.js is an open source platform built on v8 JavaScript runtime (also used in the Google Chrome web browser). It is suitable for building fast, scalable network applications \cite{nodeweb}. It provides an event-driven architecture and a non-blocking I/O API\@. Almost all of the \emph{node.js} bindings are asynchronous therefore programmers have to use the event-driven approach that leads to good scalability of the system.

  \emph{Node.js} applications are written in the JavaScript language, which is a high level, dynamic, untyped and interpreted language. The language features of JavaScript and the networking readiness of \emph{node.js} makes it a very good candidate to prototype a networking tool. As discussed in Section~\ref{sec:pc-component}, the network component of the toolchain will mainly be talking to the L4 system over the Ethernet, it was a good match to the feature spec of \emph{node.js}, thus node.js is used to build the component.

  \begin{figure}
  \includegraphics[width=1\textwidth]{figures/network-component-architecture}
  \caption{Architecture of Network Component modules and functions}
  \label{fig:networkComponent}
  \end{figure}

  \subsection{Main App}
  Figure~\ref{fig:networkComponent} gives a quick overview of the several components in the network component. The main app is the entry point of the component. It does the task of bootstrapping the component and starting different modules. The first task of the component is to parse the \gls{xml} file that is generated by the tms-sim tool (as mentioned in Section~\ref{sec:tmssim}). The \gls{xml} file of task description is internally converted into \gls{json} because the JavaScript language supports \gls{json} natively and the microkernel component is also configured to receive task descriptions in \gls{json} format.

  After the parsing of the task descriptions, the main app extracts the list of packages that the task description mentions. The packages are binaries of the tasks that would be sent to the microkernel component for execution. After this step, the main app creates an instance of the dom0-client and then dom0-client is responsible for all the communication with the microkernel component.

  \subsubsection{dom0 client}
  The dom0 client class is responsible for a number of tasks related to communication with the microkernel component. Section~\ref{sec:network} discusses the networking protocol for the communication between the PC component and the microkernel component. So, first of all, the dom0 client acts as an interface for communication between the components and has the implementation of the protocol for the communication. Moreover, it abstracts this protocol and provides intuitive methods that can be used to send commands to the microkernel networking component.

  The dom0 client uses the native \emph{node.js} \textbf{net} module to create a TCP client that could connect to the dom0 server. Since \emph{node.js} is well suited for event-based architectures, the dom0 client class is also modeled in a similar manner. It extends the \textbf{EventEmitter} class, which is a core \emph{node.js} class to implement event emitters and event handlers. \textbf{EventEmitters} are very similar to the publish-subscribe architecture where publishing is similar to emitting an event and subscribing is similar to listening for an event or attaching an event handler. The \textbf{EventEmitter} class provides number of methods among which we use the following methods: \cite{nodeEmitterweb}

  \begin{description}
    \item{\textbf{emitter.on(event, listener)}} \textendash{} The emitter.on method adds a listener (a function) to the list of listeners for the specified event.

    \item{\textbf{emitter.emit(event[,arg1][,arg2][,…]}} \textendash{} This method executes each of the listeners in order with the supplied list of arguments. It returns true if there are any listeners for the event and false if there are no listeners attached to that particular event.
    \end{description}

\begin{figure}
\includegraphics[width=1\textwidth]{figures/dom0-events}
\caption{Events emitted by the dom0 client}
\label{fig:dom0-events}
\end{figure}

Using the methods provided by the EventEmitter class, the dom0 client implements the event-driven architecture. As seen in Figure~\ref{fig:dom0-events}, the dom0 client emits the following events.

    \begin{description}
      \item{\textbf{connected}} \textendash{} This event is emitted when the dom0 client has successfully connected to the dom0 server. Once this event is received, then the main app can start sending requests or commands to the server.
      \item{\textbf{sendNextBinary}} \textendash{} Since the sending of binaries is also done asynchronously, the dom0 client maintains a queue of the binaries to be sent. Whenever the dom0 server has received the previous binary and is ready to receive the next binary it sends a message to the dom0 client. On receiving this message, the dom0 client emits the sendNextBinary event that means the dom0 client can now send the next task from the queue.
      \item{\textbf{monitor}} \textendash{} This event is emitted when the dom0 client has received some monitoring data. This event has the monitored data as the event parameters. On receiving this event, the listeners can start processing the monitoring data.
    \end{description}

\section{Microkernel/L4 Component}
The microkernel we use for the toolchain is the Fiasco.OC microkernel. \emph{Fiasco.OC} is a microkernel that belongs to the L4 family of microkernels. L4 started as a kernel designed from the ground up with performance as the primary goal. The L4 kernel has two main philosophies; first, to put only necessary function in kernel. This identifies it as a microkernel. Second, is that no policy will be there in the kernel; the kernel shall only implement mechanisms \cite{L4tudosweb}. Today, L4 signifies a family of microkernels rather than one single kernel.

\emph{Fiasco.OC} is a microkernel that supports real-time, time-sharing and virtualization applications \cite{fiascoweb}. The microkernel is the only program that runs in privileged processor mode. The microkernel provides various kernel services, which are implemented as kernel objects, these kernel objects can be referenced by tasks. These kernel object references are called as \textbf{capabilities} in the Fiasco.OC\@. Using capabilities, tasks in the userspace can call kernel level functions and can moreover pass other tasks the capabilities that they own.

\begin{figure}
\includegraphics[width=1\textwidth]{figures/l4re-basic-structure}
\caption{Basic system structure of L4Re showing most important system components. \cite{l4redocs}}
\label{fig:l4re-basic-structure}
\end{figure}

The L4 Runtime Environment (L4Re) provides services (like program loading and memory management) that make it easy to create applications for the Fiasco.OC\@. On top of the services, L4Re also provides access to libraries like the C/C++ environment, libstdc++, pthreads and virtual file-system environment \cite{l4reweb}. The libraries make it convenient to write applications and services for the Fiasco.OC microkernel. The different significant components involved in the L4Re are shown in figure\ref{fig:l4re-basic-structure}. The two most important components of the L4Re are the root pager:\textbf{Sigma0} and the root task:\textbf{Moe}. Sigma0 is primarily responsible for resolving page faults for the root task \cite{L4Servers}. The root task is the first useful task that runs on L4Re, which is moe, in general. Moe is the root task for L4Re and it provides resource management services to applications running on top of it \cite{moe}.

Moe generally starts the init process \textbf{Ned}, and provides access to the resources managed by Moe and Sigma0. As showing in figure\ref{fig:l4re-basic-structure},  Ned is the init process that coordinates the startup of services and bootstraps the communication channels for them. Ned provides a Lua scripting environment with L4Re and ELF Loader bindings, which is used to write a configuration script. This configuration script specifically bootstraps the system, sets the right communication channel and an initial environment for tasks with the required set of capabilities \cite{ned}.

The basic method of communication between different tasks in the L4-based system is the \gls{ipc}. \gls{ipc} can be of several types depending on the implementation and the system. In case of the Fiasco.OC/L4Re, they use a synchronous model for \gls{ipc}. It is a client-server based communication model where both the services have to be available for the communication to take place. The basic way of creating a \gls{ipc} communication in the Fiasco.OC/L4Re is using capabilities and providing the server with the server capabilities and the client getting access to the \gls{ipc} channel for the communication. The most convenient way of doing it is by taking advantage of the LUA scripting capabilities provided by ned, that is responsible for starting tasks and bootstrapping the communication channels.

\begin{figure}
\includegraphics[width=1\textwidth]{figures/l4-start-call-structure}
\caption{Overview of different L4Re components, their hierarchy and the comunication channel between them \cite{mcc}}
\label{fig:l4-start-call-structure}
\end{figure}

  As we discussed in Section~\ref{sec:microkernel-component}, the microkernel component is composed of two distinct sub-components, the networking component and the monitoring component. The networking component is an extension of the dom0 package, thus called as dom0. The monitoring component is implemented in a package named as libmon. Both of these packages and how they are related is described in the following sections.

  \subsection{dom0}
  dom0 is a package that came out of the work by Stark in \cite{thesisStark}; the work focused on task management between interconnected microkernel instances. On a very basic level dom0 is a network component and exposes number of interfaces to communicate with the L4 kernel and different packages and services running in it. It is capable of receiving LUA code over the network and runs it in the system via Ned.\ This execution of Lua code is facilitated by an IPC communication channel between dom0 and Ned. Another important function by the dom0 package is that it accepts a binary over the network and can start the binary on the Fiasco.OC/L4Re instance when required. Similarly, it has an IPC channel with the L4Re package as well; this communication channel is mostly used for sharing the binaries that have been sent to dom0 over the network.

 The dom0 server saves the binaries that are sent over the network in a shared dataspace. The process of starting this binary involves a number of communication channels between dom0, Ned and the l4re. The involved communication channels can be seen in figure~\ref{fig:communicationChanneldom0}. dom0 sends the command to Ned which identifies starting of a task, the task is then requested by L4re via the shared dataspace. In this way dom0 facilitates the communication for starting a task between Ned and L4Re.

  \begin{figure}
  \includegraphics[width=1\textwidth]{figures/dom0-communication-channels}
  \caption{Overview of start procedure of different components involved in starting and monitoring tasks. Dashed line shows the communication IPC Channels between the components. Based on the communication diagram in \cite{thesisStark}}
  \label{fig:communicationChanneldom0}
  \end{figure}

  \subsubsection{Task description and binary management}
  As we discussed above, dom0 is capable of accepting a binary over the network and starting it when required. One of the limitations of the dom0 initially was that it could accept only a single task binary as it had only a single dataspace for the binary. Another limitation of dom0, based on our requirements was that it was not setup to receive a set of task descriptions and start the tasks when requested.

  Building on the basic feature set of dom0, we made a number of changes and addition to it to fit to our requirements. First of all, dom0 is extended to accept a task description in \gls{json} format. It uses the JSMN parser to parse the \gls{json} file received over the network. JSMN is a minimal \gls{json} parser written in C and is well suited for resource-limited projects where time and space complexities are of concern \cite{jsmn}. It would store the parsed tasks in an array and would be used to start the task as per the mentioned parameters.

  \begin{figure}
  \includegraphics[width=1\textwidth]{figures/binary-naming-scheme}
  \caption{Naming scheme for binaries received over the network}
  \label{fig:binary-naming-scheme}
  \end{figure}

  Second, dom0 would be listening for a group of binaries and will save them in a list of managed dataspaces, unlike earlier where it could take only one binary and would save it. In the initial version, since dom0 could take only task over the network, it had a static mapping to the name \textbf(network) for the task; i.e.\ the name network would be used to start the task received over the network. In our case, since we could have any number of arbitrary binaries, it required a new scheme for referring to the binaries. The l4re package was extended so that every task with a suffix of ":n" would be considered as a binary received over the network.

  \subsubsection{Monitoring client}
  It can be seen in figure~\ref{fig:communicationChanneldom0} that there is an \gls{ipc} channel between dom0 and libmon. In this case, the libmon instance would be acting as an \gls{ipc} server while dom0 would be accessing the server as a client via \gls{ipc} channels. The reasons for setting libmon as an IPC server and dom0 as an IPC client has been discussed in the following section.\

  Since dom0 needs to be talking to the monitoring server (or libmon) periodically, it makes sense for dom0 to run a separate thread. This separate thread is responsible for requests to libmon for monitoring data. The client then sends the monitored data to the dom0 client over the network. To get the periodic nature of monitoring client, the thread then goes to sleep for a specified time and comes back again to query for monitoring data. Since it doesn't add value to be querying for monitoring data when no clients are connected to the dom0 server, the monitoring thread is started only when a client is connected to the server and the thread is stopped once the client is disconnected from the server.

  \subsection{libmon}
  \label{sec:libmon}
  libmon is a prototypical implementation of a monitoring server for the Fiasco.OC microkernel. Since it is a prototypical monitoring server, the main task of it is to complete the communication loop by communicating with the kernel and returning some relevant data back to the dom0 via IPC.\ To get back some monitoring data, it talks to the L4 Scheduler kernel object and returns the idle time of the CPU in microseconds. This data is then sent back to the dom0. Because of the client-server nature of the IPC channels in L4, it was a design decision to make libmon as a monitoring server, to which other services could connect to and ask for monitoring data. Thus, since the task being performed here is of a classical server in a client-server model, it is set as an IPC server.

  In this way, the two components of the L4 are implemented, dom0 managing the network and tasks, and libmon taking care of the monitoring and communicating it with the dom0.

\section{Networking}
An important aspect of the whole toolchain setup is the network setup between the PC component and the microkernel instances. In this section we will discuss everything related to the network and networking of the toolchain components. This section is divided into two subsections, the first subsection will describe the networking setup used during the testing and implementation of the prototype; The second subsection will dive into the network protocol that is used between the several machines.

One important decision that impacts both the networking setup and the networking protocol to be used in the prototype is that the communication between the dom0-client and the dom0-server is done using TCP/IP.\

\subsection{Networking Setup}
\label{sec:networking-setup}
The first iteration of the prototype would consist of two main components as discussed earlier: the PC component and the microkernel component. The PC component would be running on Ubuntu, a Debian based Linux operating system. The Fiasco.OC/L4Re microkernel instance would be emulated using QEMU, the open source machine emulator and virtualizer. The decision for the networking setup was hugely based on the several options that were provided by QEMU as a part of the software. The several ways via which networking can be done in QEMU are described below:

  \begin{description}
    \item{\textbf{User Networking (SLIRP)}} \textendash{} SLIRP is the default networking backend available to QEMU and is the easiest to use in most of the basic cases. It provides a full TCP/IP stack within the QEMU instance and implements a virtual NAT network. While SLIRP is a very simple and fast to implement way to get network access, there are number of limitations that makes it a bad choice for our use case. First, since it creates a virtual NAT network, the guest is not directly accessible from the host that means that we would have to manually open ports and setup forwarding for each of the microkernel instance. This is one of the biggest downside of this method apart from which the documentation also mentions that SLIRP has a lot of overhead that causes the performance to be poor \cite{qemuNet}.
    \item{\textbf{Tun/Tap device}} \textendash{} By using TAP interfaces, QEMU can provide full networking capabilities between the host and the guest operating system. The feature provided by this method is exactly what we need for out prototypical implementation with a single microkernel instance. The only disadvantage of this method is that it is a little complicated to setup the TAP interface and have the connection between the two OSes. This limitation makes it time consuming and complex to automate the networking setup. Thus, on one hand this method fits our requirements but on the other hand its complexity makes it harder to automate it.
    \item{\textbf{\gls{vde}}} \textendash{} \gls{vde} is an Ethernet compliant virtual network that can be spawned over a set of physical computer over the Internet \cite{vdeweb}. In simple words, several virtual networking-devices can connect to each other using vde\_switch, which is a virtual switch. Interestingly, the vde\_switches can connect to each other using vde\_cables. Another important feature provided by \gls{vde} is that it is possible to connect the switches to a TAP interface of the host operating system. This makes it suitable to create the connection between the several devices and the guest operating system, thus making it suitable for our use case. The only limitation for using the \gls{vde} method is that it needs to have administrator permissions to be able to connect to the guest using TAP Interfaces. Apart from that, the \gls{vde} infrastructure is configured quite easily that makes it easy to automate it.
  \end{description}

\begin{figure}
\includegraphics[width=1\textwidth]{figures/networking-setup}
\caption{Networking Setup showing the components involved}
\label{fig:networkSetup}
\end{figure}

It can be seen that among the several options for the networking setup, the Virtual Distributed Ethernet method is the most suitable method. In the figure~\ref{fig:networkSetup} it can be seen how the several components are connected using the vde\_switch.

\subsection{Networking Protocol}
\label{sec:network}
  Since this is a prototypical implementation and there is no primary need of security, there is no authentication and encryption used in the communication. For all the communication, the protocol is that the first 4 bytes that will be sent over the network will be the command that the client is trying to execute. Following the command will be the size of the data or the command that is to be transferred following the data of the corresponding size.

  Most of the commands are quite simple and require a single iteration of communication. The only exception is when the client wants to send number of binaries. In this situation, after completing the reception of each binary, the server responds with an acknowledgment. On reception of this acknowledgment the dom0-client can send the next binary from its send queue. According to the protocol, it is important to send the name of the binary first, the size of binary follows it. This is finally followed by the binary of the corresponding size. To keep it simple, the binary name should be of 8 bytes, so that we don't have to send the length of the binary name before sending the name of it.

\begin{figure}
\includegraphics[width=1\textwidth]{figures/network-protocol}
\caption{Networking Protocol for commands between dom0 client and server}
\label{fig:network-protocol}
\end{figure}

  Among all the commands that dom0-client can send to a dom0-sever, the following are the most important:

  \begin{description}
    \item{\textbf{LUA}} \textendash{} This command notifies the server that a LUA command is about to be sent. The server will then wait for the length of the command that will be followed by the command itself. Once the command execution has started, it will respond with a LUA\_OK or LUA\_ERROR, which identifies if Ned has received the command successfully or not. It does not say anything about the command being executed successfully or being failed.
    \item{\textbf{TASK\_DESC}} \textendash{} This command notifies the server that a JSON task description is about to be sent. As described in the protocol earlier, the length of the JSON string is sent after the command. After this the actual JSON string is sent. The server then parses and processes the JSON string.
    \item{\textbf{SEND\_BINARIES}} \textendash{} This command signifies that the client wants to send a number of binaries to the server. The total number of binaries is sent after the command. Then for each binary the client sends the name of the binary, the binary size and the binary. It is important that the client pauses after each binary loop and wait for the server. The server has to notify the client when it is ready to receive the next binary.
    \item{\textbf{START}} \textendash{} As it is quite clear that the \textbf{TASK\_DESC} command is used to fill up the task queue in the dom0-server and the \textbf{SEND\_BINARIES} command is used to fill up the binary queue in the dataspace manager in the dom0-server. Once the server has the task descriptions and the binaries, it is ready to start the tasks and this is done by the START command. Once the dom0-server receives this command, it one-by-one empties task from the task queue and starts them.
  \end{description}

\section{Build Tooling}
\label{sec:buildTooling}
  There are a number of steps that have to be performed before one can start running the toolchain. The task can be something like compiling and linking the Fiasco.OC and L4Re code that doesn't need to be done regularly; but there are tasks like setting up the network infrastructure, which has to be performed almost every time the tool is run. Since, there are such tasks, it made sense to develop a tooling system around the components of the toolchain so that it is easy to setup the system and run it.

  This section is divided into three subsections. The first subsection discusses the build scripts around building the Fiasco.OC/L4Re and the custom packages we have built, like the dom0 and libmon. The second subsection discusses the build scripts to setup the networking and start the emulator. Finally, the third section will describe the automated building of the binaries that are used to generate load on the system.

  \subsection{Building the system}
  There are number of commands that are run while building the Fiasco.OC microkernel and L4Re Runtime Environment. This involves first building the kernel and then building the L4Re with the packages and eventually the custom packages we have built. Custom packages can be built with the L4Re if they are put in the same source tree as the L4Re but it is considered a bad practice to put the default packages and modified packages in the same folder. It conflicts when L4Re is updated and it might update the modified packages from the L4Re server.

  As a part of the build process, we have a "build.sh" script which can run in three modes: build the kernel, build l4re and build modified packages. The script is also able to build a specific modified package as well. In Listing~\ref{lst:buildsh} we can see how different arguments can be passed to the build script to start building the packages.

\begin{lstlisting}[language=bash,caption={Different variations of the build.sh build script},label={lst:buildsh}]
  $ ./build.sh kernel #builds the Fiasco.OC microkernel
  $ ./build.sh l4re #builds the L4Re runtime environment
  $ ./build.sh mod #builds all the modified packages
  $ ./build.sh mod <module_name> #builds modified module module_name
\end{lstlisting}

  \subsection{Bootstrapping and running the system}
  Once all the packages are built, the next important thing that should be made easy is setting up the network and running the system. We discussed in Section~\ref{sec:networking-setup} that we are using vde\_switch to setup the network. The network script consists of two sections, first is to setup the tap interface and second is to configure the \gls{vde} infrastructure. A snippet of the script is available in Listing~\ref{lst:networksetup}. As discussed earlier, one of the downside of the vde infrastructure with TAP is that it needs administrator privileges to run the script.

\begin{lstlisting}[language=bash,caption={Network setup script start\_network.sh},label={lst:networksetup}]
#!/bin/bash

# setup TAP
sudo vde_tunctl -u user_name
sudo ifconfig tap0 192.168.0.254 up
sudo route add -host 192.168.0.10 dev tap0

# start VDE infrastructure
vde_switch -s /tmp/switch1
vde_plug2tap -s /tmp/switch1 tap0
\end{lstlisting}

Once the network has been setup, the script "run\_nw.sh" is responsible for giving the right networking options to QEMU and then finally running the system. The most important part of the script responsible to start QEMU and send the parameters is shown in Listing~\ref{lst:runnwsh}

\begin{lstlisting}[language=bash,caption={Snippet from shell script that starts QEMU with network},label={lst:runnwsh}]
...
QEMU_OPTIONS="-net vde,sock=/tmp/switch1" \
QEMU_OPTIONS+=" -nographic" \
MAC="52:54:00:00:00:01" \
make qemu E=dom0_server_only \
  MODULES_LIST=$MODULES_LIST MODULE_SEARCH_PATH=$MODULE_SEARCH_PATH &
...
\end{lstlisting}

  \subsection{Building binaries for testing}
  For automating the building of binaries for testing, we had to modify the Makefile for packages that belonged to the binaries used to generate the load. The change in the Makefile copies the built elf binary from the build folder of Fiasco.OC/L4Re to the package directory. The build script then moves all the binaries to a command folder. This binary folder is symlinked with the network tool in the PC component because it has to read the binaries and send it to the microkernel component running in the Fiasco.OC/L4Re instance.
