\chapter{Showcase}\label{chapter:showcase}
In this chapter we discuss the showcase that was developed during the process of the thesis to verify the prototype and confirm if the required goal is fulfilled. The first part of the chapter discusses the environment in which the showcase was run. Next we discuss about the process to get the Fiasco.OC/L4Re code and packages and build them for the showcase. Finally, we discuss the toolchain from task generation, binary generation to sending tasks and receiving the monitoring data.

\section{Environment Setup}

The showcase setup is done in a Virtual Machine running Ubuntu desktop 14.04. The virtual machine is running on an Oracle vm VirtualBox 4.3.16. The whole setup is finally running on OSX 10.10.14 running on a MacBook pro 2010 version. Since, the whole setup is done on a virtual machine, it could run on any hardware and operating system that is capable of running a virtual machine manager like VirtualBox. Our choice of the host operating system was influenced by previous projects that had build and emulated L4 system in Ubuntu 14.04 and Ubuntu 14.04 is the version currently in long-term support by the Ubuntu community.

Once the basic system is setup, there are number of packages that are quite important to build the system and run the toolchain. Apart from the default tools that are installed with Ubuntu, like the GCC compiler, the following packages have to be installed: (note: package name as per ubuntu's repository have been mentioned)

  \begin{description}
\item{\textbf{Building Fiasco.OC/L4Re}} \textendash{} make, gawk, g++, binutils, pkg-config, g++-multilib, subversion
\item{\textbf{Running Fiasco.OC/L4Re}} \textendash{} qemu
\item{\textbf{Generating Task Descriptions}} \textendash{} cmake, libxml2-dev
\item{\textbf{Network Setup}} \textendash{} vde\_switch
\item{\textbf{PC Component}} \textendash{} nodejs, npm
  \end{description}

  After, the packages have been installed, there is one more step before the system is ready; The \emph{node.js} modules for the PC component have to be installed. These packages are listed in the \textbf{package.json} file and can be installed by running the command \texttt{npm install} in the root folder of the PC component i.e. the folder that contains the \textbf{package.json} file.


\begin{figure}
\includegraphics[width=1\textwidth]{figures/directory-structure}
\caption{Toolchain high-level directory structure}
\label{fig:directory-structure}
\end{figure}

\section{Getting and Building Fiasco.OC/L4Re with custom packages}

The first step for building the kernel and runtime environment is to get the packages from their subversion repository. The instructions to get the repository via subversion or downloading manually is described on the L4 website\cite{l4redownload}.

After the repository is available locally, the build scripts mentioned in~\ref{sec:buildTooling} will be useful to build the Fiasco kernel and the L4Re runtime environment. The build scripts are based on the building instructions available at \cite{fiascobuild} and \cite{l4rebuild}. It is required to make minor changes to the script depending on the environment and naming conventions in use. For the showcase, the SVN checkout is available in a folder called as \textbf(trunk) and all external packages are available in the folder \textbf(pkg). The folder hierarchy can be visually understood from figure~\ref{fig:directory-structure}.

Running the commands on listing~\ref{lst:buildfiasco} would build the kernel and l4re.

\begin{lstlisting}[language=bash,caption={Commands to build Fiasco.OC/L4Re},label={lst:buildfiasco}]
  $ ./build.sh kernel
  $ ./build.sh l4re
  $ ./build.sh mod
\end{lstlisting}

Since it is not recommended to put modified packages in the same tree as L4Re packages, all modified packages are in the folder \textbf{pkg}. The modified packages are also built by the third command mentioned in~\ref{sec:buildTooling}. Moreover, the build script consists of a list of packages that have to be built in the process and it should be updated when packages are added or removed.


\begin{figure}
\includegraphics[width=1\textwidth]{figures/symlink-structure}
\caption{PC Networking tool directory symlink structure}
\label{fig:symlink-structure}
\end{figure}

\section{Generating Task Descriptions and Building Binaries}

As discussed in section~\ref{sec:tmssim}, the generation of task descriptions is done by the \emph{tms-sim} utility. The utility has a binary named as \textbf{generator} that generates the task description. It takes two parameters, the output file and the number of tasks in the task description file. It is run from the \emph{tms-sim} root directory and as per the naming convention, the task file should be called as tasks.xml as it is symlinked to the network tool. The command in~\ref{lst:taskGen} generates the tasks.xml file with 5 task descriptions.

\begin{lstlisting}[language=bash,caption={Generating tasks example command},label={lst:taskGen}]
  ~/tms-sim $ build/bin/generator -o tasks.xml -n 5
\end{lstlisting}

Building or compiling the task code to binaries requires the Fiasco.OC/L4Re to be built because it uses the same build tools. The task binaries are generally built using the help of the \emph{make.sh} shell script in the \emph{auto\_build} folder. This script contains the list of the binaries to be built and it one by one builds the binaries for each of the packages. All the binaries are then moved to the \emph{\_bin} folder in the same folder. This \emph{\_bin} folder is symlinked to the PC network tool as it uses the binaries from that folder to send it to the dom0-server component. The symlink structure of the network tool can also be visually understood by figure~\ref{fig:symlink-structure}

\section{Showcase Use-case}

The showcase consists of a prototypical use case where we want to generate a task description, generate the binaries for the tasks in the task description, send these binaries and task descriptions to the dom0-server and finally get the monitoring data back. This loop can be easily understood with the help of figure~\ref{fig:showcase-steps}. The process starts with the \emph{tms-sim} tool that is used to generate a \gls{xml} file of task descriptions. In the showcase, the task description contains five tasks. Next, according to the packages/binaries mentioned in the task description, the binaries are built using the \emph{auto\_build} build script. This script moves all the binaries to the \emph{\_bin} folder in the \emph{auto\_build}.

After the task descriptions and binaries are built, the next step is to start the tool. For the showcase, the network tool has a command line interface so that the steps from connection to the dom0-server to sending the binaries can be controlled and seen as a stepwise process. The PC network tool and the Fiasco.OC/L4Re are started. Either of the tools can be started first or second, it is rather important to initiate the connection to the Fiasco.OC/L4Re only after the dom0-server has started and is listening for connections. Once, both the processes, Fiasco.OC/L4Re and PC network tool have started, the tool can initiate the connection and send the task description and binaries. After the connection is made, the dom0-server communicates with libmon monitoring module and returns monitoring data. This data is printed on the console to show the completion of the loop. To sum up, the following activities of the tool can be seen in the showcase:
\begin{enumerate}
  \item Generate \gls{xml} task descriptions using the \emph{tms-sim} tool.
  \item Build task binaries using the \emph{auto\_build} tool.
  \item dom0 server is ready to accept task descriptions and task binaries. The dom0 server runs in a Fiasco.OC/L4Re instance that is emulated in a QEMU instance.
  \item PC network tool connects to the dom0 server. It sends task descriptions and binaries built using the \emph{tms-sim} tool and \emph{auto\_build} tool respectively.
  \item dom0 server sends minimal monitoring data back to the PC network tool.
  \item PC network tool receives the monitoring data and shows it to the console.
\end{enumerate}

\begin{figure}
\includegraphics[width=1\textwidth]{figures/showcase-steps}
\caption{Steps for running the toolchain}
\label{fig:showcase-steps}
\end{figure}
